import os

DATABASE_USER = os.getenv('DATABASE_USER', "user")
DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD', "password")
