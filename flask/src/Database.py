# -*- coding: utf-8 -*-

__version__ = '1.0'

# Import des bibliotheques standard
import json
from time import sleep

import psycopg2
from psycopg2.extras import RealDictCursor

# Import des ressources custom
import Conf
import Vault
from Item import Item


# Classe
class Database:
    """
    Objet Database pour assurer la communication
    entre l'app flask/python et la base de donnees postgresql

    :ivar _conf: configuration reseau (ip, port)
    :ivar _vault: parametres d'authentification
    """

    ###################### CONSTRUCTOR ######################
    def __init__(self):
        """
        Instancie un nouvel objet base de donnees
        """
        self.__dbHost = Conf.DATABASE_HOST
        self.__dbPort = Conf.DATABASE_PORT
        # self.__dbName = Vault.DATABASE_DBNAME
        self.__dbUser = Vault.DATABASE_USER
        self.__dbPassword = Vault.DATABASE_PASSWORD

    ###################### DEBUG ######################
    def show(self):
        """
        Methode d'affichage d'un objet instancie de la classe Database
        :return:
        """
        print(f'User : {self.get_db_user()}, host : {self.get_db_host()} : {self.get_db_port()}')

    # end show

    ###################### initial connection ######################
    def get_connection(self):
        """
        Ouverture d'une connexion a la base de donnees (parametres dans Conf.py et Vault.py)
        :return:
        """
        res = None
        while res is None:
            print("***DB connection***")
            try:
                res = psycopg2.connect(
                    host=self.__dbHost,
                    port=self.__dbPort,
                    user=self.__dbUser,
                    password=self.__dbPassword,
                )
            except Exception as e:
                print(e)
            sleep(2)
        return res

    # end get_connection

    def create_table(self):
        """
        Creation d'une premier table items
        :return:
        """
        query = 'CREATE TABLE IF NOT EXISTS items (label VARCHAR(100), price INT);'
        try:
            self.execute_query(query)
            for i in range(10):
                current_query = f"INSERT INTO items VALUES ('{str(i)}', '{i * i}');"
                self.execute_query(current_query)
        except Exception as e:
            print("Cant create table : ", e)

    # end create_table

    ###################### GETTER & SETTER ######################
    def get_db_user(self):
        return self.__dbUser

    def get_password(self):
        return self.__dbPassword

    # def get_db_name(self):
    #     return self.__dbName

    def get_db_host(self):
        return self.__dbHost

    def get_db_port(self):
        return self.__dbPort

    ###################### MAIN QUERY METHODS ######################
    def get_all(self):
        """
        Recuperation de l'ensemble des donnes d'une table

        :return: String format json
        :rtype: str

        :raises Exeption e (sql syntax, connexion issue)
        """
        query = "SELECT * FROM items;"
        try:
            conn = self.get_connection()
            # RealDictCursor aide a la mise au format json
            cur = conn.cursor(cursor_factory=RealDictCursor)
            cur.execute(query)
            rows = cur.fetchall()
            cur.close()
            conn.commit()
            return json.dumps(rows)
        except Exception as e:
            print(e)
            return []

    # end get_all

    def add_item(self, new_item: Item):
        # TODO : label str & price int
        """
        Enregistrement d'un nouvel item dans la base de donnees
        
        :param new_item: le nouvel item a ajouter en base de donnees

        :return: int id de creation
        :rtype: int

        :raises Exeption e (sql syntax, connexion issue)
        """
        new_label = new_item.get_label()
        new_price = new_item.get_price()
        query = f"INSERT INTO items VALUES ('{new_label}', '{new_price}');"
        self.execute_query(query)

    # end add_item

    def update_item(self, item_label, new_price):
        # TODO : l'item existe ? new_price est un int ?
        """
        Mise a jour d'un item existant dans la base de donnees

        :param item_label: le label de l'item a modifier
        :param new_price: la nouvelle valeur du prix de l'item selectionne

        :return: int id de l'item en cours pour valider la mise a jour
        :rtype: int

        :raises Exeption e (sql syntax, connexion issue)
        """
        query = f"UPDATE items SET price = {new_price} WHERE label = '{item_label}'"
        self.execute_query(query)

    # end update_item

    def delete_item(self, item_label):
        # TODO : l'item existe ?

        """
        Suppression d'un item existant dans la base de donnees

        :param item_label: le label de l'item a supprimer

        :return: str label de l'item en cours pour valider la suppression

        :raises Exeption e (sql syntax, connexion issue)
        """
        query = f"DELETE FROM items WHERE label = '{item_label}'"
        self.execute_query(query)

    # end delete_item

    def execute_query(self, query):
        """
        Methode generale d'execution d'une requete sql

        :param query: requete sql
        :type query: str

        :return:
        """
        conn = self.get_connection()
        cur = conn.cursor()
        cur.execute(query)
        cur.close()
        conn.commit()
        # TODO : server_status, information back to front
        # return created + new id
        # return deleted + old id
        # return updated + current id + old/new value

    # end execute_query

# end class Database
