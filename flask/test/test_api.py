import json
import pytest

import requests


class HttpRequest:

    def __init__(self):
        self.__path = '/item'
        # self.__server_url = "http://" + Conf.SERVER_HOST + ':' + Conf.SERVER_PORT + self.__path
        self.__server_url = "http://127.0.0.1:5001/item"

    def select_query(self):
        print("select * :")
        req_get = requests.get(url=self.get_url())
        # req_get = requests.get(url="http://127.0.0.1:5001/item")
        print(req_get.json())

    def insert_query(self):
        print("insert into :")

        new_item = ({"label": "new_name", "price": 3141592})
        print(new_item)
        req_post = requests.post(url=self.get_url(), json=json.dumps(new_item, indent=4))
        print("created : ", req_post.status_code)

    def delete_query(self, item_to_delete):
        print("delete :")
        req_del = requests.delete(url=self.get_url(), json=item_to_delete)
        # print("deleted : ", req_del.json())

    def update_query(self, item_to_update):
        print("Update : ", item_to_update)
        req_upd = requests.put(url=self.get_url(), json=item_to_update)

    def get_url(self):
        return self.__server_url


req = HttpRequest()

def test_am_i_working():
    assert (16+1 == 17)

def test_am_i_working2():
    assert (16 + 1 == 18)


# def get_test():
#     assert (req.select_query() == ["label", "price"])
