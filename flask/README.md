# Flask - Docker - Docker-compose

Exercice de compréhension et de mise en pratique 

## Elements :

- [ ] REST API Flask (crud simple)
- [ ] Base de données PostGreSQL

## Vanilla
- Demarrer le conteneur docker PostGreSQL fournit par l'entreprise
- Lancer l'application python
```py
python3 main.py
```
- Utiliser l'application avec Curl :
```python
curl http://127.0.0.1:5001/item
curl -i -H "Content-Type: application/json" -X POST -d '{"label": "gibson", "price": 17}' http://127.0.0.1:5001/item
# curl -X PUT http://127.0.0.1:5000/item
# curl -X DELETE http://127.0.0.1:5000/item
```
- Utiliser l'application a l'aide du fichier request_test.py

```python
import requests
class HttpRequest:

    def __init__(self):
        self.__path = '/item'
        self.__server_url = "http://" + SERVER_HOST + ':' + SERVER_PORT + self.__path
        
    def select_query(self):
        print("select * :")
        req_get = requests.get(url=self.get_url())
        print(req_get.json())
        
req = HttpRequest()
# req.select_query()
req.insert_query()
# req.select_query()
# req.delete_query("pi")
# req.select_query()

```

## Docker
- Construire l'image docker
```shell
docker build -t flask_app:1.0.0 .
```

- Demarrer les deux conteneur Docker (app Flask et DB Postgre) :
```shell
docker run -it -p 5001:5001 --expose 5432 --net flask_app flask_app:1.0.0
docker run --expose 5432 -e  POSTGRES_PASSWORD=password -e POSTGRES_USER=user host:port/postgres:14  
```
- Acceder a la base de donnees :
```shell
psql -h localhost -p 5432 -d items -U user --password
```

## Docker-compose :
```python
docker-compose up
```

