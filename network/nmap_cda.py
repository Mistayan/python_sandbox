# from nmap import nmap
#
# scan = nmap.PortScanner()
# nm.scan(hosts='192.168.1.0/24', arguments='-sn')
#
# for host in nm.all_hosts():
#     print(f'{host} is {nm[host].state()}')


####
# from scapy.all import *
# from scapy.layers.inet import IP, ICMP
#
#
# def ping_scan(hosts):
#     for host in hosts:
#         packet = IP(dst=host)/ICMP()
#         response = sr1(packet, timeout=2, verbose=False)
#         if response:
#             print(f'{host} is up')
#         else:
#             print(f'{host} is down')
#
# # hosts = ['192.168.1.1', '192.168.1.2', '192.168.1.3']
#
#
# def all_my_ips():
#     for i in range(1, 255):
#         # yield f'192.168.1.{i}'
#         yield f'10.60.65.{i}'
#
# ping_scan(list(all_my_ips()))

######
import socket

def ping(host):
    # Create a raw socket
    with socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP) as s:
        # Set a timeout for the socket
        s.settimeout(2)
        # Send an ICMP echo request to the host
        try:
            s.sendto('', (host, 1))
        except:
            print(f'bad address : {host}')

        # Wait for a response
        try:
            data, address = s.recvfrom(1024)
            return True
        except socket.timeout:
            return False

def ping_scan(hosts):
    for host in hosts:
        if ping(host):
            print(f'{host} is up')
        else:
            print(f'{host} is down')

hosts = ['10.60.65.1', '10.60.65.2', '10.60.65.3', '10.60.65.4']
ping_scan(hosts)

