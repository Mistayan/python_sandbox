import itertools

import numpy as np

UPPER = ['A', 'B', 'C']
LOWER = ['a', 'b', 'c', 'd']


def function_status(func_name):
    """
    Current method status, clean, bugged, need to rework
    :param func_name:
    :return:
    """
    status_dict = {
        'loop_in_loop': 'oof',
        'itertools_loop': 'oof',
        'group_by_loop': 'ok'
    }
    return status_dict.get(func_name, 'status : undefined')


def header(func):
    """
    prettier print table in cli
    :param func:
    :return:
    """

    def wrapper(*args):
        print(f'\n * {func.__name__}, {function_status(func.__name__)}')
        print(f'{"ARRAY1":<9}::: ASSOCIATION with {LOWER}', sep=' - ')
        return func(*args)

    return wrapper


def to_ascii(char):
    return char[1], ord(char[0])


@header
def loop_in_loop(array1, array2):
    """
    for in for loop over array1 & array2
    :param array2:
    :param array1:
    :return:
    """
    for item_1 in array1:
        print(f'\n{item_1:<10}:', end=' ')
        for item_2 in array2:
            print(f'({item_1}{item_2})', end=' ')


@header
def itertools_loop(array1, array2):
    """
    itertools product over array1 & array2
    :param array1:
    :param array2:
    :return:
    """
    for item_1, item_2 in itertools.product(array1, array2):
        print(f'{item_1:<10}: {item_2}')


@header
def group_by_loop(array1, array2):
    """
    group all item from array1 with every single item from array2
    :param array1:
    :param array2:
    :return:
    """
    array_assocation = list(itertools.product(array1, array2))
    for item_1, item_1_with_array2 in itertools.groupby(array_assocation, lambda x: x[0]):
        convert_result = list(map(to_ascii, list(item_1_with_array2)))
        print(f'{item_1:<10}: {convert_result}')


@header
def numpy_loop(array1, array2):
    """
    numpy way
    :param array1:
    :param array2:
    :return:
    """
    np_array1, np_array2 = np.array(array1), np.array(array2)
    print(np_array1, np_array2)


if __name__ == '__main__':
    loop_in_loop(UPPER, LOWER)
    itertools_loop(UPPER, LOWER)
    group_by_loop(UPPER, LOWER)
    numpy_loop(UPPER, LOWER)
